<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
get_header(); ?>

	<div class="container">

		<?php
		if ( have_posts() ) :
			?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				?>

			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumb-270', false )[0];
				?>
				<a class="post-link" href="<?php echo esc_url( get_permalink() ); ?>"><img alt="<?php the_title(); ?>" class="post-img" src="<?php echo $image; ?>"></a>
			</div><!-- .entry-content -->
		</article><!-- #post-## -->

				<?php
			endwhile;

			the_posts_navigation();
			else :
				get_template_part( 'template-parts/content', 'none' );
		endif;
			?>

	</div><!-- #primary -->

<?php
get_footer();
