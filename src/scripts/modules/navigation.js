/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function() {
    var container, menu, links, subMenus, i, len

    container = document.getElementById('site-navigation')
    if (!container) {
        return
    }

    menu = container.getElementsByTagName('ul')[0]

    // Hide menu toggle button if menu is empty and return early.
    if ('undefined' === typeof menu) {
        button.style.display = 'none'
        return
    }

    menu.setAttribute('aria-expanded', 'false')
    if (-1 === menu.className.indexOf('nav-menu')) {
        menu.className += ' nav-menu'
    }

    // Get all the link elements within the menu.
    links = menu.getElementsByTagName('a')
    subMenus = menu.getElementsByTagName('ul')

    // Set menu items with submenus to aria-haspopup="true".
    for (i = 0, len = subMenus.length; i < len; i++) {
        subMenus[i].parentNode.setAttribute('aria-haspopup', 'true')
    }

    // Each time a menu link is focused or blurred, toggle focus.
    for (i = 0, len = links.length; i < len; i++) {
        links[i].addEventListener('focus', toggleFocus, true)
        links[i].addEventListener('blur', toggleFocus, true)
    }

    /**
     * Sets or removes .focus class on an element.
     */
    function toggleFocus() {
        var self = this

        // Move up through the ancestors of the current link until we hit .nav-menu.
        while (-1 === self.className.indexOf('nav-menu')) {
            // On li elements toggle the class .focus.
            if ('li' === self.tagName.toLowerCase()) {
                if (-1 !== self.className.indexOf('focus')) {
                    self.className = self.className.replace(' focus', '')
                } else {
                    self.className += ' focus'
                }
            }

            self = self.parentElement
        }
    }

    // Submenu Open/Close
    var subMenuToggles = document.querySelectorAll('.sub-menu-toggle')
    console.log(subMenuToggles)

    for (var i = 0; i < subMenuToggles.length; i++) {
        subMenuToggles[i].addEventListener('click', function() {
            this.parentElement.classList.toggle('sub-menu-open')
        })
    }
})()
;(function() {
    var toggles = document.querySelectorAll('.menu-toggle')
    if (!toggles) {
        return
    }

    document.addEventListener('click', function(event) {
        Array.from(toggles).forEach(tog => {
            if (!event.path && !event.composedPath()) console.log('Browser does not support path or composedPath()');
            var inPath = event.path && event.path.indexOf(tog) > -1 ? true : false || event.composedPath() && event.composedPath().indexOf(tog) > -1 ? true : false

            var menu =
                tog.getAttribute('aria-controls') ||
                tog.getAttribute('data-target')
            menu = document.querySelector(`#${menu}`)
            if (!menu) {
                console.log(`Could not find menu #${menu}`)
                return
            }

            if (inPath) {
                toggleMenu(tog, menu)
            } else {
                var inMenu = event.path && event.path.indexOf(menu) > -1 ? true : false || event.composedPath() && event.composedPath().indexOf(menu) > -1 ? true : false

                if (!inMenu && tog.classList.contains('toggled')) {
                    toggleMenu(tog, menu)
                }
            }
        })
    })

    function toggleMenu(ele, menu) {
        console.log(ele)

        ele.classList.toggle('toggled')
        menu.classList.toggle('toggled')

        ele.setAttribute('aria-expanded', !!ele.getAttribute('aria-expanded'))
    }
})()
