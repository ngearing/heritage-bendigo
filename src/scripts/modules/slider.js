import Siema from 'siema'

var defaultOptions = {
	arrows: true,
	dots: false,
	autoplay: false,
	loop: true
}

// Add a function that generates pagination to prototype
Siema.prototype.addPagination = function() {
	const controls = document.createElement('div')
	controls.classList.add('controls')
	this.selector.appendChild(controls)
	for (let i = 0; i < this.innerElements.length; i++) {
		const btn = document.createElement('button')
		btn.textContent = i
		btn.addEventListener('click', () => {
			this.goTo(i)
		})

		if (i === this.config.startIndex) {
			btn.classList.add('current')
		}
		controls.appendChild(btn)
	}

	this.config.onChange = function() {
		const btns = this.selector.querySelectorAll('.controls button')
		for (let index = 0; index < btns.length; index++) {
			const element = btns[index]
			if (element.classList.contains('current')) {
				element.classList.remove('current')
			}

			if (index === this.currentSlide) {
				element.classList.add('current')
			}
		}
	}
}

// Extend prototype with method that adds arrows to DOM
// Style the arrows with CSS or JS — up to you mate
Siema.prototype.addArrows = function() {
	// make buttons & append them inside Siema's container
	this.prevArrow = document.createElement('button')
	this.nextArrow = document.createElement('button')
	this.prevArrow.textContent = 'previous slide'
	this.nextArrow.textContent = 'next slide'
	this.prevArrow.classList.add('arrow', 'prev')
	this.nextArrow.classList.add('arrow', 'next')
	this.selector.appendChild(this.prevArrow)
	this.selector.appendChild(this.nextArrow)

	// event handlers on buttons
	this.prevArrow.addEventListener('click', () => this.prev())
	this.nextArrow.addEventListener('click', () => this.next())
}

const sliders = document.querySelectorAll('.slider')

for (const slider of sliders) {
	var options = slider.getAttribute('data-options') || {}
	if (typeof options == 'string') {
		options = JSON.parse(options)
	}
	options = Object.assign({ selector: slider }, defaultOptions, options)

	var mySlider = new Siema(options)

	if (options.arrows) {
		mySlider.addArrows()
	}

	if (options.dots) {
		mySlider.addPagination()
	}
	console.log(mySlider)
}
