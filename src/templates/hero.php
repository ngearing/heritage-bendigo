<?php

$img = get_field( 'hero_image' );
$txt = get_field( 'hero_text' );

if ( ! $img ) {
	$img = get_theme_file_uri( 'src/images/tulips.jpg' );
} else {
	$img = wp_get_attachment_image_url( $img, 'hero-lrg' );
}

?>

<div class="hero" style="background-image: url( <?php echo $img; ?>);">
	<?php if ( $txt ) : ?>
		<h2><?php echo $txt; ?></h2>
	<?php endif; ?>
</div>
