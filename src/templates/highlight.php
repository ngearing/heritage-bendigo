<?php
/**
 * Post highlight template.
 */

?>
<div class="highlight">
	<div class="post-img">
		<a href="<?php echo get_the_permalink( $post_id ); ?>">
			<?php echo get_the_post_thumbnail( $post_id, 'hero-sml' ); ?>
		</a>
	</div>
	<h3><?php echo $cat_title; ?></h3>
	<div class="text">
		<h4><?php echo get_the_title( $post_id ); ?></h4>
		<p class="excerpt"><?php readmore( get_the_excerpt( $post_id ) ); ?></p>
	</div>
</div>
