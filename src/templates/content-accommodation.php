<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php
	$images = get_field( 'gallery' );
	if ( $images ) {
		slider( $images, 'large' );
	}
	?>

	<?php room_features( get_the_ID() ); ?>

	<div class="key-details">
		<?php the_field( 'key_details' ); ?>
	</div>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<aside class="room-actions">
		<h4>Reserve This Room</h4>

		<div class="widget-reservation-box">
			<div class="room-price-widget">
				<p class="from">Room From</p>
				<h3 class="price"><?php the_field( 'price_from' ); ?></h3>
				<p class="price-detail"> Per Night</p>
			</div>

			<a href="<?php the_field( 'booking_link' ); ?>">Book Now</a>
		</div>
	</aside>

</article><!-- #post-## -->
