<?php

$page_header_img = isset( $page_header_img ) ? $page_header_img : get_field( 'page_header_image' );
$page_header_txt = isset( $page_header_txt ) ? $page_header_txt : get_the_title();

if ( ! $page_header_img ) {
	$page_header_url = get_theme_file_uri( 'src/images/placeholder-banner.jpg' );
} else {
	$page_header_url = wp_get_attachment_image_url( $page_header_img, 'header-lrg' );
}
?>

<div class="page-header" style="background-image: url( <?php echo $page_header_url; ?>);">

	<?php
	printf(
		'<%s class="page-title">%s</%1$s>',
		is_singular() || ( is_page() && ! is_front_page() ) ? 'h1' : 'h2',
		$page_header_txt
	);
	?>

</div>
