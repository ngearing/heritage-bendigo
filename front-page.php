<?php
get_header(); ?>

<?php get_template_part( 'src/templates/hero' ); ?>

<div class="features">
	<span class="feature"><i class="fas fa-wifi"></i> <span class="text">Free Wifi</span></span>
	<span class="feature"><i class="fas fa-utensils"></i> <span class="text">Delicious Breakfast</span></span>
	<span class="feature"><i class="fas fa-swimmer"></i> <span class="text">Solar Heated Pool</span></span>
	<span class="feature"><i class="fas fa-car"></i> <span class="text">Secure Parking</span></span>
</div>

<?php
set_query_var( 'page_header_img', get_field( 'property_section_image' ) );
set_query_var( 'page_header_txt', get_field( 'property_section_text' ) );
get_template_part( 'src/templates/page-header' );
?>

<?php accommodation_shortcode(); ?>

<section class="availability-rates">
	<div class="container">

		<h2>Availability and Rates</h2>

		<div class="booking-widget">
			<script src="https://secure.staah.com/cal/js/jquery-ui.js"></script>
			<script type="text/javascript" language="javascript" src="https://secure.staah.com/common-cgi/properties/widget/staahbookingcbgrp_mlsearch_J.php?id=13597&checkintxt=Arrival&showcheckout=yes&checkouttext=Departure&curdate=yes&promocode=no&promotxt=Promocode&promocode=no&dateformat=dd M yy&buttontext=Book Now&nonights=1&unk=2678"></script>
		</div>
		<div class="tripadvisor-logo">
			<a href="com.au/Hotel_Review-g255347-d255764-Reviews-The_Heritage_Bendigo-Bendigo_Greater_Bendigo_Victoria.html" target="_blank" rel="noopener">
				<?php
				ob_start();
				require get_theme_file_path( 'src/images/tripadvisor.svg' );
				echo ob_get_clean();
				?>
			</a>
		</div>
		<div class="testimonials">
			<h2>What our guests have to say</h2>
			<div class="slider">
				<?php
				$posts = get_posts(
					[
						'post_type' => 'testimonial',
					]
				);

				foreach ( $posts as $post ) {
					printf(
						'<div class="testimonial slide">
							<blockquote>
								%s
								<footer>- %s</footer>
							</blockquote>
						</div>',
						get_the_content( false, false, $post ),
						get_the_title( $post )
					);
				}
				?>
			</div>
		</div>

	</div>
</section>

<section class="highlights">
	<div class="container">

		<?php
		$cats = [
			'to-see',
			'to-do',
			'we-love',
		];
		foreach ( $cats as $cat ) {
			$cat = get_term_by( 'slug', $cat, 'category' );
			if ( ! $cat ) {
				continue;
			}

			$posts = get_posts(
				[
					'category'       => $cat->term_id,
					'posts_per_page' => 3,
					'fields'         => 'ids',
				]
			);
			if ( ! $posts ) {
				return;
			}

			set_query_var( 'cat_title', $cat->name );
			?>
			<div class="slider" data-options='{"arrows":false,"dots":true}'>
				<?php
				foreach ( $posts as $post_id ) {
					set_query_var( 'post_id', $post_id );
					get_template_part( 'src/templates/highlight' );
				}
				?>
			</div>
			<?php
		}
		?>
	</div>

	<svg class="bg">
		<use href="#bg" width="100%" height="100%" />
	</svg>
	<svg class="bg">
		<use href="<?php echo get_theme_file_uri( 'src/images/bg.svg#bg' ); ?>" width="100%" height="100%" />
	</svg>

</section>

<section class="map">
	<a href="https://goo.gl/maps/VSyYukMejKz" target="_blank" rel="noopener">
		<img src="<?php echo get_theme_file_uri( 'src/images/map.jpg' ); ?>" alt="<?php printf( 'Map of %s\'s location.', get_bloginfo() ); ?>">
	</a>
</section>

<?php
get_footer();
