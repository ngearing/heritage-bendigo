const path = require('path')

module.exports = {
	parser: 'postcss-scss',
	plugins: {
		autoprefixer: {
			grid: true
		},
		'postcss-normalize': {},
		'postcss-critical-css': {
			outputPath: path.join(__dirname, 'dist'),
			minify: true,
			preserve: false
		}
	}
}
