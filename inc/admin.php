<?php

/**
 * Remove Admin pages.
 */
function ggstyle_remove_admin_pages() {
	remove_menu_page( 'edit-comments.php' );          // Comments.php.
	remove_menu_page( 'link-manager.php' );           // Links.
}
add_action( 'admin_menu', 'ggstyle_remove_admin_pages' );

function ggstyle_edit_admin_columns( $columns ) {

	return $columns;
}
add_filter( 'manage_posts_columns', 'ggstyle_edit_admin_columns' );

function ggstyle_hide_comments_column( $columns ) {

	if ( isset( $columns['comments'] ) ) {
		unset( $columns['comments'] );
	}
	return $columns;
}

add_action(
	'admin_init',
	function() {
		foreach ( get_post_types() as $type ) {
			add_filter( "manage_{$type}_posts_columns", 'ggstyle_hide_comments_column' );
		}
	}
);
