<?php


function accommodation_shortcode() {
	wp_enqueue_style( 'ggstyle-rooms' );
	?>
<section class="rooms">
<div class="container">
	<?php
	$rooms = get_posts(
		[
			'post_type'      => 'accommodation',
			'posts_per_page' => 6,
			'fields'         => 'ids',
		]
	);

	foreach ( $rooms as $key => $room ) {
		?>
		<article class="room">
			<?php
			$images = get_field( 'gallery', $room );
			if ( $images ) {
				$images = array_slice( $images, 0, 3 );
				slider( $images, 'hero-sml' );
			} else {
				printf(
					"<div class='slider' data-options='{\"arrows\":false,\"dots\":true}'><div class='slide'><img class='slide-img placeholder' src='%s' alt='Placeholder Image'></div></div>",
					get_theme_file_uri( 'src/images/placeholder-icon.png' )
				);
			}
			?>

			<h4 class="title"><?php echo get_the_title( $room ); ?></h4>
			<?php room_features( $room ); ?>
			<p class="excerpt">
				<?php
				$text = get_field( 'key_details', $room );
				readmore( $text, 12, false, false );
				?>
			</p>
			<a class="link" href="<?php echo get_the_permalink( $room ); ?>">View Details</a>
			<a class="link book" href="<?php echo get_field( 'booking_link', $room ); ?>" target="_blank" rel="noopener">Book Now</a>
		</article>
		<?php
	}
	?>
	</div>
	<?php
	ob_start();
	require get_theme_file_path( 'src/images/bg.svg' );
	echo ob_get_clean();
	?>
</section>
	<?php
}
add_action( 'init', 'register_shortcodes' );

function register_shortcodes() {
	add_shortcode( 'accommodation', 'accommodation_shortcode' );
}
