<?php



add_action(
	'init',
	function() {
		register_post_type(
			'accommodation',
			array(
				'labels'        => array(
					'name'          => __( 'Accommodation', 'quitenicebooking' ),
					'singular_name' => __( 'Accommodation', 'quitenicebooking' ),
					'add_new'       => _x( 'Add Room', 'accommodation', 'quitenicebooking' ),
					'add_new_item'  => __( 'Add New Room', 'quitenicebooking' ),
				),
				'public'        => true,
				'menu_icon'     => 'dashicons-grid-view',
				'menu_position' => 5, // below Posts
				'has_archive'   => true,
				'rewrite'       => array(
					'slug' => __( 'accommodation', 'quitenicebooking' ),
				),
				'supports'      => array( 'title', 'editor', 'thumbnail', 'revisions' ),
			)
		);

		register_taxonomy(
			'accommodation-tags',
			'accommodation',
			[
				'labels'            => [
					'name' => 'Tags',
				],
				'public'            => true,
				'show_admin_column' => true,
			]
		);

		register_post_type(
			'testimonial',
			array(
				'labels'        => array(
					'name'          => __( 'Testimonials', 'qns' ),
					'singular_name' => __( 'Testimonial', 'qns' ),
					'add_new'       => __( 'Add Testimonial', 'qns' ),
					'add_new_item'  => __( 'Add New Testimonial', 'qns' ),
				),
				'public'        => true,
				'menu_position' => 5,
				'menu_icon'     => 'dashicons-star-filled',
				'rewrite'       => array(
					'slug' => __( 'testimonial', 'qns' ),
				),
				'supports'      => array( 'title', 'editor' ),
			)
		);
	}
);
