<?php

class CustomWalkerPage extends Walker_Page {

	public $current_el = false;

	public function start_el( &$output, $page, $depth = 0, $args = array(), $current_page = 0 ) {
		$this->current_el = $page;

		parent::start_el( $output, $page, $depth, $args, $current_page );
	}

	/**
	 * Outputs the beginning of the current level in the tree before elements are output.
	 *
	 * @since 2.1.0
	 *
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Used to append additional content (passed by reference).
	 * @param int    $depth  Optional. Depth of page. Used for padding. Default 0.
	 * @param array  $args   Optional. Arguments for outputting the next level.
	 *                       Default empty array.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {

		if ( $args['has_children'] ) {
			$page    = $this->current_el;
			$output .= "<button class='sub-menu-toggle' value='Show sub menu for $page->post_title'></button>";
		}

		if ( isset( $args['item_spacing'] ) && 'preserve' === $args['item_spacing'] ) {
			$t = "\t";
			$n = "\n";
		} else {
			$t = '';
			$n = '';
		}
		$indent = str_repeat( $t, $depth );

		$output .= "{$n}{$indent}<ul class='children'>{$n}";
	}
}
