<?php
/**
 * ggstyle functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ggstyle_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ggstyle, use a find and replace
	 * to change 'ggstyle' to the name of your theme in all the template files.
	 */

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Add image sizes
	 */
	add_image_size( 'hero-lrg', 1920, 540 );
	add_image_size( 'hero-med', 1024, 430 );
	add_image_size( 'hero-sml', 768, 560 );
	add_image_size( 'header-lrg', 1920, 200 );
	add_image_size( 'header-med', 1024, 200 );
	add_image_size( 'header-sml', 768, 200 );
	add_image_size( 'slider-sml', 250, 200, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'primary' => esc_html__( 'Primary', 'ggstyle' ),
			'footer'  => esc_html__( 'Footer', 'ggstyle' ),
		)
	);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support(
		'post-formats',
		array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		)
	);

	/*
	 * Enable theme support Theme Logo
	 * See https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'     => 300,
			'width'      => 300,
			'flex-width' => true,
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'ggstyle_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);
}
add_action( 'after_setup_theme', 'ggstyle_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ggstyle_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ggstyle_content_width', 640 );
}
add_action( 'after_setup_theme', 'ggstyle_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ggstyle_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'ggstyle' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'ggstyle' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'ggstyle_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ggstyle_scripts() {
	// Register styles we may use.
	wp_register_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css', [], '5.9.0', 'screen' );
	wp_register_style( 'typekit', 'https://use.typekit.net/amm0xyg.css', [], '1.0.0', 'screen' );

	// Load styles / scripts for every page.
	wp_enqueue_style( 'ggstyle', get_theme_file_uri( 'dist/main.css' ), ['font-awesome', 'typekit'], filemtime( get_theme_file_path( 'dist/main.css' ) ), 'screen' );
	wp_enqueue_script( 'ggstyle', get_theme_file_uri( 'dist/main.js' ), [], filemtime( get_theme_file_path( 'dist/main.js' ) ), true );

	if ( is_front_page() && file_exists( get_theme_file_path('dist/front-page.css') ) ) {
		wp_enqueue_style( 'ggstyle-front-page', get_theme_file_uri('dist/front-page.css'), [], filemtime( get_theme_file_path('dist/front-page.css')), 'screen' );
		wp_enqueue_script( 'ggstyle-front-page', get_theme_file_uri( 'dist/front-page.js' ), [], filemtime( get_theme_file_path( 'dist/front-page.js' ) ), true );
	}

	if ( is_single() && file_exists( get_theme_file_path('dist/single.css') ) ) {
		wp_enqueue_style( 'ggstyle-single', get_theme_file_uri('dist/single.css'), [], filemtime( get_theme_file_path('dist/single.css')), 'screen' );
	}
}
add_action( 'wp_enqueue_scripts', 'ggstyle_scripts' );

add_action( 'wp_footer', function() {
	
} );

/**
 * Pre get posts for altering the main query.
 */
function ggstyle_pre_get_posts( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	return $query;
}
add_filter( 'pre_get_posts', 'ggstyle_pre_get_posts', 1, 10 );

/**
 * Add Submenu Arrows
 */
function add_arrow( $output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $depth === 0 ) {
		if ( in_array( 'menu-item-has-children', $item->classes ) ) {
			$output .= '<span class="sub-menu-toggle"></span>';
		}
	}
	return $output;
}
add_filter( 'walker_nav_menu_start_el', 'add_arrow', 10, 4 );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/shortcodes.php';
require get_template_directory() . '/inc/custom-walker-page.php';

/**
 * Admin related functions and customizations.
 */
require get_theme_file_path( 'inc/custom-post-types.php' );

/**
 * Admin related functions and customizations.
 */
require get_theme_file_path( 'inc/admin.php' );
