<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ggstyle
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<h1>
	<?php
	if ( is_front_page() ) :
		?>
 I am a homepage
		<?php
else :
	?>
		<?php the_title(); ?><?php endif; ?></h1>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ggstyle' ),
					'after'  => '</div>',
				)
			);
			?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
