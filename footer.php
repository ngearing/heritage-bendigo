<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
	</main><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">


		<div class="container">
			<div class="site-about">
				<h4><?php bloginfo(); ?></h4>
				<p>Comfortable accommodation in Bendigo’s Golden Square – conveniently located near the centre of
Bendigo. The Bendigo Heritage Motor Inn offers 24 spacious ground floor hotel suites with 32″ LCD
cable TV. Mini Bars and high speed Wi-Fi. Other amenities at this Bendigo motel are a licensed on-site
restaurant, swimming pool and conference facilities.</p>
			</div>

			<div class="site-contact">
				<h4>Get in touch</h4>
				<p>259 High Street, Golden Square, Bendigo, VIC 3550 Australia</p>
				<p><a href="mailto:info@theheritagebendigo.com.au"><i class="icon-envelope"></i> info@theheritagebendigo.com.au</a></p>
				<p><a href="tel:03 5442 2788"><i class="icon-phone"></i> 03 5442 2788</a></p>
				<p><a href="https://www.facebook.com/bendigoheritage/" target="_blank" rel="noopener"><i class="icon-facebook"></i> Facebook</a><a href="https://www.instagram.com/theheritagebendigo" target="_blank" rel="noopener"><i class="icon-instagram"></i> Instagram</a></p>
			</div>
		</div>

		<div class="site-info">
			<div class="container">
			<div class="site-copy">
				<?php printf( esc_html__( '&copy; %1$s %2$s. All rights reserved.', 'ggstyle' ), date( 'Y' ), "<a href='" . esc_url( home_url( '/' ) ) . "'>" . get_bloginfo() . '</a>' ); ?>
			</div>

			<div class="site-credits">
				<a href="https://greengraphics.com.au/" rel="noopener" target="_blank">Web design.</a>
			</div>
		</div><!-- .container -->
		</div><!-- .site-info -->


	</footer><!-- #colophon -->

</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>
