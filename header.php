<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ggstyle
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo get_bloginfo( 'name' ); ?> | <?php echo get_bloginfo( 'description' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<script src='https://watchmyrate.com/wmr.php?secure_code=VnyXwULcfxilf3Um5sgaJQ&date_cmformat=dd-M-yy'></script> 
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'ggstyle' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<div class="container">

			<div class="top-actions">
				<a href="tel:03 5442 2788" class="action tel"><i class="fas fa-phone"></i> <span class="text">03 5442 2788</span></a>
				<a href="mailto:info@theheritagebendigo.com.au" class="action email"><i class="fas fa-envelope"></i> <span class="text">info@theheritagebendigo.com.au</span></a>
			</div>

			<div class="site-branding">
				<?php if ( is_home() || is_front_page() ) : ?>
				<h1 class="site-title">
					<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/src/images/logo.png"/></a>
				</h1>
				<?php else : ?>
				<p class="site-title">
					<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/src/images/logo.png"/></a>
				</p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false">
				<span class="menu-toggle-box">
					<span class="menu-toggle-inner"></span>
				</span>
			</button>

		</div><!-- .container -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php
			$menu_options = [
				'theme_location' => 'primary',
				'menu_id'        => 'primary-menu',
			];

			if ( ! has_nav_menu( 'primary' ) ) {
				$menu_options['walker'] = new CustomWalkerPage();
			}

			wp_nav_menu( $menu_options );
			?>
		</nav><!-- #site-navigation -->

	</header><!-- #masthead -->

	<main id="main" class="site-main" role="main">
